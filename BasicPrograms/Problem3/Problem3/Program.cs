﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem3
{
    class Program
    {
        static void Main(string[] args)
        {
            bool exit = false;
            int option = 0;
            int accountno;

            List<Account> accounts = new List<Account>();

            accounts.Add(new Account(12345, "abcd", 10000));
            accounts.Add(new Account(23456, "efgf", 100));
            accounts.Add(new Account(98765, "pqrs", 2000));
            accounts.Add(new Account(45678, "wxyz", 5000));
            accounts.Add(new Account(56789, "lnmo", 500));       

            double cash = 0;
            char a;
            string password = "";
            Account account = null;


            while (true)
            {
                Console.Write("\nAccount No: ");
                accountno = Convert.ToInt32(Console.ReadLine());

                if ((account = accounts.Find(x => x.AccountNo == accountno)) == null)
                {
                    Console.WriteLine("\nAccount No does not exist!");
                    Console.ReadKey();
                    continue;
                }

                Console.Write("Password: ");
                while ((a = Console.ReadKey(true).KeyChar) != (char)13)
                {
                    Console.Write("*");
                    password += a;
                }

                if (account.Password != password)
                {
                    Console.WriteLine("\nWrong Password!");
                }
                else
                    break;
            }

            Console.Clear();
            

            Console.WriteLine("\t*********** Welcome to Bank Application ***********");
            while (!exit)
            {
                Console.ReadKey();
                Console.Clear();
                Console.WriteLine("\n\nBank Operations:");
                Console.WriteLine("1. Balance Checking");
                Console.WriteLine("2. Cash Withdrawal");
                Console.WriteLine("3. Cash Deposition");
                Console.WriteLine("4. Exit");
                Console.Write("Enter Option: ");
                try
                {
                    option = Convert.ToInt32(Console.ReadLine());
                }
                catch (Exception)
                {
                    Console.WriteLine("\nEnter Integer Only!");
                    continue;
                }

                switch (option)
                {
                    case 1:
                        Console.Clear();
                        Console.WriteLine("\nBalance: $" + account.GetBalance());
                        break;

                    case 2:
                        Console.Clear();
                        Console.Write("\nEnter amount to be withdrawn: $");

                        try
                        {
                            cash = Convert.ToInt32(Console.ReadLine());
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("\nEnter Number Only!");
                            break;
                        }

                        if (account.CashWithdraw(cash))
                        {
                            Console.WriteLine("\nCash Withdrawn!");
                        }
                        else
                        {
                            Console.WriteLine("\nEnough balance not present!");
                        }
                        break;

                    case 3:
                        Console.Clear();
                        Console.Write("\nEnter amount to be deposited: $");

                        try
                        {
                            cash = Convert.ToInt32(Console.ReadLine());
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("\nEnter Number Only!");
                            break;
                        }

                        account.CashDeposit(cash);
                        Console.WriteLine("\nSuccessfully Deposited!");
                        break;

                    case 4:
                        Console.Clear();
                        Console.WriteLine("\nExiting... (Enter any key)");
                        exit = true;
                        break;

                    default:
                        Console.Clear();
                        Console.WriteLine("\nInvalid option! Try again!");
                        break;
                }
            }
        }
    }
}
