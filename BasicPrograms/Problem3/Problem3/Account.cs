﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem3
{
    class Account
    {
        public int AccountNo { get; set; }
        public string Password { get; set; }
        private double balance;


        public Account(int accountno, string password, double balance)
        {
            this.AccountNo = accountno;
            this.Password = password;
            this.balance = balance;
        }


        public bool CashWithdraw(double cash)
        {
            if (cash > balance)
            {
                return false;
            }
            balance -= cash;
            return true;
        }


        public void CashDeposit(double cash)
        {
            balance += cash; 
        }

        
        public double GetBalance()
        {
            return balance;
        }

        
    }
}
