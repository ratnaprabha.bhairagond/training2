﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem2
{
    class Program
    {
        static void Main(string[] args)
        {
            char grade = '\0';

            Console.Write("Enter Grade: ");
            try
            {
                grade = Convert.ToChar(Console.ReadLine());
            }
            catch (Exception)
            {
                Console.WriteLine("Enter a character only!");
            }

            Console.WriteLine(GradeDescription(grade));

            Console.ReadLine();
        }

        static string GradeDescription(char grade)
        {
            switch (grade)
            {
                case 'A':
                case 'a':
                    return ("\nPassed! Excellent Grade! Keep it up!");
                case 'B':
                case 'b':
                    return ("\nPassed! Good Grade!");
                case 'C':
                case 'c':
                    return ("\nPassed! Average Grade!");
                case 'D':
                case 'd':
                    return ("\nPassed! Bad Grade!");
                case 'E':
                case 'e':
                    return ("\nFailed! Very Bad Grade!");
                default:
                    return ("\nNot a vaild Grade!");
            }
        }
    }
}
