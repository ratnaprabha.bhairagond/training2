﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1b
{
    class Program
    {
        static void Main(string[] args)
        {
            string Characters = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            int lengthOfPassword;
            int index;
            string password = null;

            Random random = new Random();

            Console.Write("Input length of password: ");
            lengthOfPassword = Convert.ToInt32(Console.ReadLine());

            Parallel.For(0, lengthOfPassword, i =>
            {
                index = random.Next(62);
                password += Characters[index];
            });

            Console.WriteLine("Password: " + password);
            Console.ReadLine();
        }
    }
}
