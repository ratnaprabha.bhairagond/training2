﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem1
{
    class Program
    {
        static void Main(string[] args)
        {
            int lengthOfPassword;
            int number;
            char smallAlphabet;
            char capitalAlphabet;
            string password = null;
            int choice;

            Random random = new Random();

            Console.Write("Input length of password: ");
            lengthOfPassword = Convert.ToInt32(Console.ReadLine());

            Parallel.For(0, lengthOfPassword, i => {               
                choice = random.Next(3);
                switch (choice)
                {
                    case 0:
                        number = random.Next(10);
                        password += number;
                        break;
                    case 1:
                        smallAlphabet = (char)(random.Next(97, 123));
                        password += smallAlphabet;
                        break;
                    case 2:
                        capitalAlphabet = (char)(random.Next(65, 91));
                        password += capitalAlphabet;
                        break;
                    default:
                        break;
                }
            });

            Console.WriteLine("Password: " + password);
            Console.ReadLine();
        }
    }
}
