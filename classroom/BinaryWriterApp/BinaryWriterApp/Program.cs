﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BinaryWriterApp
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            //Open or create a file
            FileStream fs = File.OpenWrite(@"c:\training2\product.txt");

            BinaryWriter writer = new BinaryWriter(fs);

            //product id as int
            writer.Write(123);

            //product name as string
            writer.Write("Product 1");

            //product price as double
            writer.Write(34.55);

            //close writer stream
            writer.Close();
            fs.Close();

            Console.WriteLine("Product details are written to a file!");
            */


            using (FileStream fs = File.OpenRead(@"c:\training2\product.txt"))
            {
                BinaryReader reader = new BinaryReader(fs);

                Console.WriteLine("Product ID: {0}", reader.ReadInt32());
                Console.WriteLine("Product Name: {0}", reader.ReadString());
                Console.WriteLine("Product Price: {0}", reader.ReadDouble());

                reader.Close();
                fs.Close();
            }
        }
    }
}
