﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileIOApp
{
    class Program
    {
        static void Main(string[] args)
        {
            DirectoryInfo di = new DirectoryInfo(@"c:\Windows");

            Console.WriteLine("Listing contents of {0} directory: ", di.FullName);

            foreach (DirectoryInfo dirr in di.GetDirectories())
            {
                //Details of every file
                Console.WriteLine("Directory Name: " + dirr.Name);
                foreach (FileInfo file in dirr.GetFiles())
                {
                    Console.WriteLine("File Name: " + file.Name);
                    Console.WriteLine("File size in (bytes): " + file.Length);
                    Console.WriteLine("Creation Time: " + file.CreationTime);
                    Console.WriteLine();
                }
                Console.WriteLine();
            }
        }
    }
}
