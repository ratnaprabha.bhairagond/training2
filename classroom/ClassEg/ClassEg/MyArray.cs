﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassEg
{
    public class MyArray
    {
        public MyArray()
        {

        }

        public void NonStaticMethod()
        {

        }

        public static void StaticMethod()
        {

        }
    }
    public struct Point
    {
        int x, y;
        public Point(int a, int b)
        {
            x = a;
            y = b;
        }
        public int X
        {
            get
            {
                return x;
            }
            set
            {
                x = value;
            }
        }
        public void abc()
        {

        }
    }
}
