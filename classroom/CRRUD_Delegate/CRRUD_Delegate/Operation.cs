﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRRUD_Delegate
{
    delegate void Operations(int id);
    delegate void InputOutput();
    class Operation
    {
        public event InputOutput eCreate;
        public event Operations eSearch;
        public event InputOutput eDisplay;
        public event InputOutput dCreate;
        public event Operations dSearch;
        public event InputOutput dDisplay;
        public event Operations eDelete;
        public event Operations eUpdate;
        public event Operations dDelete;
        public event Operations dUpdate;

        public void Operate(int i, int j, int id = 0)
        {
            switch (i)
            {
                case 1:
                    switch (j)
                    {
                        case 1:
                            dCreate();
                            break;
                        case 2:
                            dDisplay();
                            break;
                        case 3:
                            dSearch(id);
                            break;
                        case 4:
                            dUpdate(id);
                            break;
                        case 5:
                            dDelete(id);
                            break;
                        default:
                            break;
                    }
                    break;
                case 2:
                    switch (j)
                    {
                        case 1:
                            eCreate();
                            break;
                        case 2:
                            eDisplay();
                            break;
                        case 3:
                            eSearch(id);
                            break;
                        case 4:
                            eUpdate(id);
                            break;
                        case 5:
                            eDelete(id);
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
