﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRRUD_Delegate
{
    class Program
    {

        static List<Employee> E = new List<Employee>();
        static List<Department> D = new List<Department>();


        static void Main(string[] args)
        {
            Operation o = new Operation();
            o.dCreate += DeptCreate;
            o.dDisplay += DeptDisplay;
            o.dSearch += DeptSearch;
            o.dUpdate += DeptUpdate;
            o.dDelete += DeleteDept;

            o.eCreate += EmpCreate;
            o.eDisplay += EmpDisplay;
            o.eSearch += EmpSearch;
            o.eUpdate += EmpUpdate;
            o.eDelete += DeleteEmp;


            bool i = true;
            int ch, a, id;
            while (i)
            {
                Console.Clear();
                Console.WriteLine("\nChoose one of the following: ");
                Console.WriteLine("1. Department Operations");
                Console.WriteLine("2. Employee Operations");
                Console.WriteLine("3. Exit");
                Console.Write("Enter option: ");
                ch = Convert.ToInt32(Console.ReadLine());
                Console.Clear();
                switch (ch)
                {
                    case 1:
                    case 2:
                        Console.WriteLine("\nChoose Operation: ");
                        Console.WriteLine("1. Create");
                        Console.WriteLine("2. Display all");
                        Console.WriteLine("3. Search");
                        Console.WriteLine("4. Update");
                        Console.WriteLine("5. Delete");
                        Console.Write("Enter option: ");
                        a = Convert.ToInt32(Console.ReadLine());
                        Console.Clear();
                        switch (a)
                        {
                            case 1:
                            case 2:
                                o.Operate(ch, a);
                                break;
                            case 3:
                            case 4:
                            case 5:
                                Console.Write("\nEnter ID: ");
                                id = Convert.ToInt32(Console.ReadLine());
                                Console.Clear();
                                o.Operate(ch, a, id);
                                break;
                            default:
                                break;
                        }
                        break;
                    case 3:
                        Console.WriteLine("\nExiting...(Enter Any Key)");
                        Console.ReadKey();
                        i = false;
                        break;
                    default:
                        Console.WriteLine("\nEnter valid option!!");
                        Console.ReadKey();
                        break;
                }
            }
        }


        public static void DeleteDept(int did)
        {
            Department d;
            Employee e;
            d = D.Find(x => x.DepartID == did);
            e = E.Find(x => x.DepartID == did);
            if (d != null)
            {
                if (e != null)
                {
                    Console.WriteLine("\nEmployees still exist in Department ID {0}! Cannot delete!", did);
                }
                else
                {
                    D.Remove(d);
                    Console.WriteLine("\nDeleted!");
                }
            }
            else
            {
                Console.WriteLine("\nDepartment ID {0} not present!", did);
            }
            Console.ReadKey();
        }


        public static void DeleteEmp(int eid)
        {
            Employee e;
            e = E.Find(x => x.EmpID == eid);
            if (e != null)
            {
                E.Remove(e);
                Console.WriteLine("\nDeleted!");
            }
            else
            {
                Console.WriteLine("\nEmployee ID {0} not present!", eid);
            }
            Console.ReadKey();
        }

        public static void EmpUpdate(int eid)
        {
            bool f = true;
            int did, ch;
            Department d;
            Employee e = null;
            e = E.Find(x => x.EmpID == eid);
            if (e != null)
            {
                Console.Write("\nEnter Employee Name: ");
                e.EmpName = Console.ReadLine();
                while (f)
                {
                    Console.WriteLine("\nEnter Designation: ");
                    Console.WriteLine("1. Employee");
                    Console.WriteLine("2. Manager");
                    Console.WriteLine("3. President");
                    Console.Write("Enter Option: ");
                    ch = Convert.ToInt32(Console.ReadLine());
                    switch (ch)
                    {
                        case 1:
                            e.Design = "Employee";
                            f = false;
                            break;
                        case 2:
                            e.Design = "Manager";
                            f = false;
                            break;
                        case 3:
                            e.Design = "President";
                            f = false;
                            break;
                        default:
                            Console.WriteLine("\nInvalid! Try Again!");
                            break;
                    }
                }
                Console.Write("\nEnter Basic Salary: ");
                e.BasicSalary = Convert.ToDouble(Console.ReadLine());
                Console.Write("\nEnter Department ID: ");
                did = Convert.ToInt32(Console.ReadLine());
                d = D.Find(x => x.DepartID == did);
                if (d == null)
                {
                    Console.WriteLine("\nDepartment ID {0} does not exist!", did);
                }
                else
                {
                    e.DepartID = did;
                    Console.WriteLine("\nSuccessfully Updated!");
                }
                Console.WriteLine("\nUpdated!");
            }
            else
            {
                Console.WriteLine("\nEmployee ID {0} not present!", eid);
            }
            Console.ReadKey();
        }

        public static void DeptUpdate(int did)
        {
            Department d = null;
            d = D.Find(x => x.DepartID == did);
            if (d != null)
            {
                Console.Write("\nEnter Department Name: ");
                d.DepartName = Console.ReadLine();
                Console.Write("\nEnter Location: ");
                d.Location = Console.ReadLine();
                Console.WriteLine("\nSuccessfully Updated!");
            }
            else
            {
                Console.WriteLine("\nDepartment ID {0} not present!", did);
            }
            Console.ReadKey();
        }

        public static void EmpSearch(int eid)
        {
            Employee e = null;
            e = E.Find(x => x.EmpID == eid);
            if (e != null)
            {
                Console.WriteLine(e);
            }
            else
            {
                Console.WriteLine("\nEmployee ID {0} not present!", eid);
            }
            Console.ReadKey();
        }

        public static void DeptSearch(int did)
        {
            Department d = null;
            d = D.Find(x => x.DepartID == did);
            if (d != null)
            {
                Console.WriteLine(d);
            }
            else
            {
                Console.WriteLine("\nDepartment ID {0} not present!", did);
            }
            Console.ReadKey();
        }

        public static void EmpCreate()
        {
            bool f = true;
            string name, des = "";
            double bs;
            int did, ch;
            Department d;
            Console.Write("\nEnter Employee Name: ");
            name = Console.ReadLine();

            while (f)
            {
                Console.WriteLine("\nEnter Designation: ");
                Console.WriteLine("1. Employee");
                Console.WriteLine("2. Manager");
                Console.WriteLine("3. President");
                Console.Write("Enter Option: ");
                ch = Convert.ToInt32(Console.ReadLine());
                switch (ch)
                {
                    case 1:
                        des = "Employee";
                        f = false;
                        break;
                    case 2:
                        des = "Manager";
                        f = false;
                        break;
                    case 3:
                        des = "President";
                        f = false;
                        break;
                    default:
                        Console.WriteLine("\nInvalid! Try Again!");
                        break;
                }
            }
            Console.Write("\nEnter Basic Salary: ");
            bs = Convert.ToDouble(Console.ReadLine());
            Console.Write("\nEnter Department ID: ");
            did = Convert.ToInt32(Console.ReadLine());
            d = D.Find(x => x.DepartID == did);
            if (d == null)
            {
                Console.WriteLine("\nDepartment ID {0} does not exist!", did);
            }
            else
            {
                E.Add(new Employee(name, des, bs, did));
                Console.WriteLine("\nSuccessfully Added!");
            }
            Console.ReadKey();
        }

        public static void DeptCreate()
        {
            string name, location;
            Console.Write("\nEnter Department Name: ");
            name = Console.ReadLine();
            Console.Write("\nEnter Location: ");
            location = Console.ReadLine();
            D.Add(new Department(name, location));
            Console.WriteLine("\nSuccessfully Added!");
            Console.ReadKey();
        }

        public static void EmpDisplay()
        {
            foreach (var Employee in E)
            {
                Console.WriteLine("\n" + Employee);
            }
            Console.ReadKey();
        }

        public static void DeptDisplay()
        {
            foreach (var Department in D)
            {
                Console.WriteLine("\n" + Department);
            }
            Console.ReadKey();
        }
    }
}
