﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HashtableTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable hashtable = CollectWords();
            DisplayHashtable(hashtable);
        }

        private static void DisplayHashtable(Hashtable hashtable)
        {
            Console.WriteLine("\nHashtable contains:\n{0, -12}{1, -12}", "Key:", "Value:");
            foreach (var key in hashtable.Keys)
            {
                Console.WriteLine("{0, -12}{1, -12}", key, hashtable[key]);
            }

            Console.WriteLine("\nSize: {0}", hashtable.Count);
        }

        private static Hashtable CollectWords()
        {
            Hashtable table = new Hashtable();
            Console.WriteLine("Enter a string: ");
            string input = Console.ReadLine();

            string[] words = Regex.Split(input, @"\s+");

            foreach (var word in words)
            {
                string wordKey = word.ToLower();
                if (table.ContainsKey(wordKey))
                {
                    table[wordKey] = ((int)table[wordKey]) + 1;
                }
                else
                {
                    table.Add(wordKey, 1);
                }
            }
            return table;
        }
    }
}
