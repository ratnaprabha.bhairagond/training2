﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsApp
{
    class Program
    {
        private static int[] intValues = { 1, 2, 3, 4, 5, 6 };
        private static double[] doubleValues = { 8.4, 9.3, 0.2, 7.9, 3.4 };
        private static int[] intValuesCopy;
        static void Main(string[] args)
        {
            //1. Array
            intValuesCopy = new int[intValues.Length];
            Console.WriteLine("Initial Array Values:\n");
            PrintArrays();

            //sort doubleValues
            Array.Sort(doubleValues);

            //copy values from intValues to intValuesCopy array
            Array.Copy(intValues, intValuesCopy, intValues.Length);

            Console.WriteLine("\nArray values after Sort and Copy:\n");
            PrintArrays();

            //search 5 in intValues array
            int x = 5;
            int result = Array.BinarySearch(intValues, x);
            if (result >= 0)
            {
                Console.WriteLine("{0} found at element {1} in intValues", x, result);
            }
            else
            {
                Console.WriteLine("{0} not found in intValues", x);
            }

            //search 8783 in intValues
            result = Array.BinarySearch(intValues, 8783);
            if (result>=0)
            {
                Console.WriteLine("{0} found at element {1} in intValues", 8783, result);
            }
            else
            {
                Console.WriteLine("{0} not found in intValues", 8783);
            }
        }

        private static void PrintArrays()
        {

            Console.WriteLine("doubleValues: ");
            IEnumerator enumerator = doubleValues.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Console.WriteLine(enumerator.Current + " ");
            }
            Console.WriteLine("\nintValues: ");
            enumerator = intValues.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Console.WriteLine(enumerator.Current + " ");
            }
            Console.WriteLine("\nintValuesCopy: ");
            foreach (var element in intValuesCopy)
            {
                Console.WriteLine(element + " ");
            }
        }
    }
}
