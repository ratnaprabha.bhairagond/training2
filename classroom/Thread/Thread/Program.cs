﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ThreadAp
{
    public delegate int BinaryOp(int x, int y);

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Main() is invoked on thread no: {0}", Thread.CurrentThread.ManagedThreadId);
            BinaryOp o = Add;
            Console.WriteLine("Calling Main() at {0}", DateTime.Now.ToString());

            IAsyncResult iar = o.BeginInvoke(10, 12, null, null);


            Console.WriteLine("Back to Main() at {0}", DateTime.Now.ToString());
            Console.WriteLine("Doing more work in Main()!");

            int result = o.EndInvoke(iar);

            Console.WriteLine("{0} + {1} is {2}", 10, 12, result);
            Console.ReadLine();
        }

        static int Add(int a, int b)
        {
            Console.WriteLine("Inside Add() at {0}", DateTime.Now.ToString());
            Console.WriteLine("Add() is invoked on thread no: {0}", Thread.CurrentThread.ManagedThreadId);
            Thread.Sleep(5000);
            Console.WriteLine("Returning to Main() at {0}", DateTime.Now.ToString());
            return a + b;
        }
    }
}
