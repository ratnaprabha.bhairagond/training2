﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebuggerApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Account account1 = new Account(50.00M);

            Console.WriteLine("account1 balance: {0:c}", account1.Balance);

            decimal withdrawalAmount;

            Console.Write("Enter withdrawal amount for account1: ");

            withdrawalAmount = Convert.ToDecimal(Console.ReadLine());

            Console.WriteLine("\nWithdrawing {0:C} from account1 balance", withdrawalAmount);

            account1.Debit(withdrawalAmount);

            Console.WriteLine("account1 balance is: {0:C}", account1.Balance);
            Console.WriteLine();

            Console.Write("Enter Credit amount for account1: ");

            decimal creditAmount = Convert.ToDecimal(Console.ReadLine());

            Console.WriteLine("\nadding {0:C} to account balance", creditAmount);
            account1.Credit(creditAmount);

            Console.WriteLine("account1 balance is: {0:C}", account1.Balance);
            Console.WriteLine();

            Console.ReadLine();
        }
    }
}
