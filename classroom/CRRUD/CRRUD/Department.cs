﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRRUD
{
    class Department
    {
        public int DepartID { get; set; }
        public string DepartName { get; set; }
        public string Location { get; set; }
        private static int count;

        public Department(string name, string location)
        {
            DepartID = ++count;
            DepartName = name;
            Location = location;
        }

        public void PrintDept()
        {
            Console.WriteLine("\nDepartment ID: \t\t{0}", DepartID);
            Console.WriteLine("Department Name: \t{0}", DepartName);
            Console.WriteLine("Location: \t\t{0}\n", Location);
        }

        public override string ToString()
        {
            return "\nDepartment ID: \t\t" + DepartID + "\nDepartment Name: \t" + DepartName + "\nLocation: \t\t" + Location;
        }

        public static int DepCount()
        {
            return count;
        }
    }
}
