﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRRUD
{
    class Employee
    {
        public int EmpID { get; set; }
        public string EmpName { get; set; }
        public string Design { get; set; }
        public double BasicSalary { get; set; }
        public double HRA { get; set; }
        public double ProFund { get; set; }
        public double GrossSalary { get; set; }
        public double NetSalary { get; set; }
        public int DepartID { get; set; }
        private static int count;

        public Employee(string name, string design, double bsalary, int deptid)
        {
            count++;
            EmpID = count;
            EmpName = name;
            Design = design;
            BasicSalary = bsalary;
            DepartID = deptid;
            HRA = (0.8 * BasicSalary);
            ProFund = (0.12 * BasicSalary);
            GrossSalary = BasicSalary + HRA + 1250;
            NetSalary = GrossSalary - (1500 + ProFund);
        }

        public override string ToString()
        {
            return "\nEmployee ID: \t\t" + EmpID + "\nEmployee Name: \t\t" + EmpName + "\nDesignation: \t\t" + Design + "\nBasic Salary: \t\t" + BasicSalary + "\nHRA: \t\t\t" + HRA + "\nProvident Fund: \t" + ProFund + "\nGross Salary: \t\t" + GrossSalary + "\nNet Salary: \t\t" + NetSalary + "\nDepartment ID: \t\t" + DepartID;
        }

        public static int EmpCount()
        {
            return count;
        }
    }
}
