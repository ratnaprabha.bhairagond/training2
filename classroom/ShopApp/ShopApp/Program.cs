﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShopApp
{
    class Program
    {
        static void Main(string[] args)
        {

            MessageBox.Show("Welcome to Shopping Application!");

            List<Product> productlist;
            List<Cart> cartlist;

            //Creating object of Data Context to get information from Database
            using (ShopDBDataClassesDataContext db = new ShopDBDataClassesDataContext())
            {
                //Importing tables from db to local variables
                productlist = db.Products.ToList();
                cartlist = db.Carts.ToList();
                db.ExecuteCommand("DELETE FROM Cart");
            }

            MainMenu.Call(productlist, cartlist);

            using (ShopDBDataClassesDataContext db = new ShopDBDataClassesDataContext())
            {
                //Updating db
                db.Carts.InsertAllOnSubmit(cartlist);
                db.SubmitChanges();
            }

        }
    }
}
