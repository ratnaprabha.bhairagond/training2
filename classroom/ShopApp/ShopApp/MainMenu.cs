﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShopApp
{
    //Class for Main Menu
    public class MainMenu
    {
        //Method for Printing Main Menu
        //Accepts productlist and cartlist
        public static void Call(List<Product> productlist, List<Cart> cartlist)
        {

            int choice, productid, choice1;
            bool value = true;
            DialogResult dialogResult;

            while (value)
            {
                main:
                Console.Clear();
                Console.WriteLine("\n************ MAIN MENU ************");
                Console.WriteLine("\nChoose one of the folowing: ");
                Console.WriteLine("1. List Products");
                Console.WriteLine("2. List Products by Category");
                Console.WriteLine("3. Add Product to Cart");
                if (!Carts.IsEmpty(cartlist))
                {
                    Console.WriteLine("4. Edit Quantity");
                    Console.WriteLine("5. Cart Details");
                    Console.WriteLine("6. Remove Product from Cart");
                    Console.WriteLine("7. Cart Summary");
                    Console.WriteLine("8. Place Order"); 
                }
                Console.WriteLine("Enter 0 to Exit");
                Console.Write("\nEnter Option: ");
        
                if ((choice = Helper.IntException()) == -1)
                    continue;
                    

                Console.Clear();
                
                switch (choice)
                {
                    case 0:
                        dialogResult = MessageBox.Show("Do you want to exit?", "Exit", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            value = false;
                        }
                        break;

                    case 1:
                        Products.ListProducts(productlist);
                        Helper.Back();
                        break;

                    case 2:
                        Products.ListCategory(productlist);
                        break;

                    case 3:
                        while (true)
                        {
                            Console.Clear();
                            Console.WriteLine("\n1. Add Product");
                            Console.WriteLine("2. Go Back to Main Menu");
                            Console.Write("\nEnter Option: ");
                            if ((choice1 = Helper.IntException()) == -1)
                                continue;
                            switch (choice1)
                            {
                                case 1:
                                    break;
                                case 2:
                                    goto main;
                                default:
                                    MessageBox.Show("\nInvalid Option!");
                                    continue;
                            }
                            Console.Clear();
                            Console.Write("\nEnter Product ID: ");
                            if ((productid = Helper.IntException()) == -1)
                                continue;
                            Carts.AddProduct(productid, productlist, cartlist);
                        }

                    case 4:
                        while (true)
                        {
                            Console.Clear();
                            if (!Carts.CartDetails(cartlist))
                                break;
                            Console.WriteLine("\n\n1. Edit Quantity");
                            Console.WriteLine("2. Go Back to Main Menu");
                            Console.Write("\nEnter Option: ");
                            if ((choice1 = Helper.IntException()) == -1)
                                continue;
                            switch (choice1)
                            {
                                case 1:
                                    break;
                                case 2:
                                    goto main;
                                default:
                                    MessageBox.Show("\nInvalid Option!");
                                    continue;
                            }
                            Console.Clear();
                            Carts.CartDetails(cartlist);
                            Console.Write("\n\nEnter Product ID: ");
                            if ((productid = Helper.IntException()) == -1)
                                continue;
                            Carts.EditQuantity(productid, cartlist);
                        }
                        break;

                    case 5:
                        Carts.CartDetails(cartlist);
                        Helper.Back();
                        break;

                    case 6:
                        while (true)
                        {
                            Console.Clear();
                            if (!Carts.CartDetails(cartlist))
                                break;
                            Console.WriteLine("\n\n1. Remove Product");
                            Console.WriteLine("2. Go Back to Main Menu");
                            Console.Write("\nEnter Option: ");
                            if ((choice1 = Helper.IntException()) == -1)
                                continue;
                            switch (choice1)
                            {
                                case 1:
                                    break;
                                case 2:
                                    goto main;
                                default:
                                    MessageBox.Show("\nInvalid Option!");
                                    continue;
                            }
                            Console.Clear();
                            Carts.CartDetails(cartlist);
                            Console.Write("\n\nEnter Product ID: ");
                            if ((productid = Helper.IntException()) == -1)
                                continue;
                            Carts.RemoveProduct(productid, cartlist);
                        }
                        break;

                    case 7:
                        Carts.CartSumm(cartlist);
                        Helper.Back();
                        break;

                    case 8:
                        if (!Carts.CartDetails(cartlist))
                            break;
                        Console.WriteLine();
                        Carts.CartSumm(cartlist);
                        dialogResult = MessageBox.Show("Do you want to place order?", "PlaceOrder", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            Carts.PlaceOrder(cartlist);
                        }
                        break;

                    default:
                        MessageBox.Show("\nInvalid Option!");
                        break;
                }
            }
        }
    }
}
