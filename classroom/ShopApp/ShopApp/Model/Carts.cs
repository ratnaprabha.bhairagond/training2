﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShopApp
{

    //Class for Cart Operations
    class Carts
    {

        //Method to Add Product to cartlist
        //Accepts product id, productlist and cartlist
        public static void AddProduct(int productid, List<Product> productlist, List<Cart> cartlist)
        {
            Product product = null;
            Cart cart = null;

            //Exception for Product ID not present in productlist
            try
            {
                product = productlist.Where(x => x.Id == productid).Single(); 
                DialogResult dialogResult = MessageBox.Show("Do you want to add Product?", "Add", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    if ((cart = Helper.CartException(productid, cartlist)) == null)
                    {
                        //Add Product to cartlist
                        cartlist.Add(new Cart { Id = product.Id, Name = product.Name, Price = product.Price, Count = 1 });

                    }
                    else
                    {
                        //Increment count if Product already present in cartlist
                        cart.Count += 1;
                    }
                    MessageBox.Show("Successfully Added!");
                }
            }
            catch (Exception)
            {
                MessageBox.Show(String.Format("Product ID {0} does not exist!", productid));
            }
        }



        //Method to print details from cartlist
        //Accepts cartlist
        //Returns false if Empty, else returns true
         public static bool CartDetails(List<Cart> cartlist)
        {
            if(IsEmpty(cartlist))
            {
                return false;
            }
            Console.WriteLine("\nID\tName\t\t\tPrice\t\tQuantity\tTotal");
            foreach (var product in cartlist)
            {
                Console.WriteLine("\n{4, -7}\t{0, -20}\t${1, -12}\t{2, -12}\t${3, -12}", product.Name, product.Price, product.Count, product.Count * product.Price, product.Id);
            }
            return true;
        }



        //Method to remove product from cartlist
        //Accepts product id to remove and cartlist
        public static void RemoveProduct(int productid, List<Cart> cartlist)
        {
            Cart cart = null;

            if ((cart = Helper.CartException(productid, cartlist)) == null)
            {
                MessageBox.Show(String.Format("Product ID {0} not present in cart!", productid));
                return;
            }
            DialogResult dialogResult = MessageBox.Show("Do you want to remove?", "Remove", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                cartlist.Remove(cart);
                MessageBox.Show("\nSuccessfully Removed!");
            }
        }



        //Method to Print Cart Summary
        //Accepts cartlist
        public static void CartSumm(List<Cart> cartlist)
        {
            decimal total = 0;
            int quantity = 0;
            foreach (var product in cartlist)
            {
                total += product.Price * product.Count;
                quantity += product.Count;
            }
            Console.WriteLine("\nTotal Items: {0}", quantity);
            Console.WriteLine("Total Price: ${0}", total);
        }



        //Method to edit quantity of particular product in cartlist
        //Accepts productid and cartlist
        public static void EditQuantity(int productid, List<Cart> cartlist)
        {
            Cart cart = null;
            int quantity;

            if ((cart = Helper.CartException(productid, cartlist)) == null)
            {
                MessageBox.Show(String.Format("Product ID {0} not present in cart!", productid));
                return;
            }

            Console.Write("\nEnter Quantity: ");

            if ((quantity = Helper.IntException()) == -1)
                return;

            DialogResult dialogResult = MessageBox.Show("Do you want to edit quantity?", "Edit", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                cart.Count = quantity;
                MessageBox.Show("Successfully Edited!");
            }
        }



        //Method to Delete all from cartlist
        //Accepts cartlist
        public static void PlaceOrder(List<Cart> cartlist)
        {
            Console.WriteLine();
            cartlist.Clear();
            MessageBox.Show("\nOrder Placed!");
        }



        //Method to check if cartlist is Empty
        //Accepts cartlist
        //Returns true if cartlist Empty, else returns false
        public static bool IsEmpty(List<Cart> cartlist)
        {
            if (cartlist.Count() == 0)
            {
                return true;
            }
            return false;
        }
    }
}
