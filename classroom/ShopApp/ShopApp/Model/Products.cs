﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShopApp
{

    //Class for Product Operations
    class Products
    {

        //Method to list products from table productlist
        //Accepts productlist
        public static void ListProducts(List<Product> productlist)
        {
            Helper.PrintProducts(productlist);
        }



        //Method to list category wise products from productlist
        //Accepts productlist
        public static void ListCategory(List<Product> productlist)
        {
            int choice = 0;
            string category = null;
            while (true)
            {
                Console.Clear();
                Console.WriteLine("\n1. Waterports");
                Console.WriteLine("2. Soccer");
                Console.WriteLine("3. Chess");
                Console.WriteLine("4. Go Back to Main Menu");
                Console.Write("\nEnter Choice: ");
                if ((choice = Helper.IntException()) == -1)
                    return;
                Console.Clear();
                switch (choice)
                {
                    case 1:
                        category = "Waterports";
                        break;
                    case 2:
                        category = "Soccer";
                        break;
                    case 3:
                        category = "Chess";
                        break;
                    case 4:
                        return;
                    default:
                        MessageBox.Show("\nInvalid Option!");
                        return;
                }

                //Query to get products of particular category
                var result = from x in productlist
                             where x.Category == category
                             select x;

                Helper.PrintProducts(result.ToList());

                Console.Write("\nPress Enter to Go Back ");
                Console.ReadKey();
            }
        }
    }
}
