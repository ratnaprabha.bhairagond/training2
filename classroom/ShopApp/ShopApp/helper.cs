﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShopApp
{
    public class Helper
    {

        //Method for Data Type Int Exception
        //Returns -1 if exception, else returns int value 
        public static int IntException()
        {
            string input;
            try
            {
                input = Console.ReadLine();
                if (input.Length == 0)
                    return -1;
                return Convert.ToInt32(input);
            }
            catch (Exception)
            {
                MessageBox.Show("Please enter integer only!");
                return -1;
            }
        }


        //Method to print go back to MainMenu 
        public static void Back()
        {
            Console.Write("\nPress Enter to go back to Main Menu ");
            Console.ReadKey();
        }


        //Method to print Result from productlist
        public static void PrintProducts(List<Product> productlist)
        {
            foreach (var item in productlist)
            {
                Console.WriteLine("\nID:\t\t{0}\nName:\t\t{1}\nDescription:\t{2}\nCategory:\t{3}\nPrice:\t\t${4}", item.Id, item.Name, item.Description, item.Category, item.Price);
                Console.WriteLine();
            }
        }


        //Method for Exception for product not present in cartlist
        //Accepts productid and cartlist
        //Returns null if Product not found, else returns Product from cartlist
        public static Cart CartException(int productid, List<Cart> cartlist)
        {
            try
            {
                Cart cart = cartlist.Where(x => x.Id == productid).Single();
                return cart;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
