﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQtoSQL
{
    class Program
    {
        static void Main(string[] args)
        {
            ObjectQuery();
        }
        static void ObjectQuery()
        {
            var db = new TrainingDBDataClassesDataContext();
            db.Log = Console.Out;

            var result = from p in db.Products
                         where p.Category=="Chess"
                         select p;

            foreach (var item in result)
            {
                Console.WriteLine("ID: {0}\tName: {1}", item.Id, item.Name);
            }
        }
    }
}
