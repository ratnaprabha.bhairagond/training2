﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionApp
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Type t = typeof(int);
            Console.WriteLine("IsAbstract: {0}", t.IsAbstract);
            Console.WriteLine("IsClass: {0}", t.IsClass);
            Console.WriteLine("IsEnum: {0}", t.IsEnum);
            Console.WriteLine("IsPrimitive: {0}", t.IsPrimitive);
            Console.WriteLine("IsValueType: {0}", t.IsValueType);
            */


            /*
            Console.WriteLine("Please enter full path of dll to be loaded: ");
            string asm = Console.ReadLine();

            //load the assembly of given path
            Assembly assembly = Assembly.LoadFile(@asm);

            //get all the types
            Type[] types = assembly.GetTypes();

            //enumerate through this collection
            foreach (Type t in types)
            {
                Console.WriteLine("Type Name: {0}", t.Name);
                Console.WriteLine("Is Class?: {0}", t.IsClass);
                Console.WriteLine();

                if (t.IsClass)
                {
                    Console.WriteLine("Methods in this class: ");
                    foreach (MethodInfo mi in t.GetMethods())
                    {
                        Console.WriteLine("Name: {0}", mi.Name);
                    }
                }
            }
            */


            Assembly assembly = Assembly.LoadFile(@"C:\training2\classroom\ReflectionApp\TripStackLib\bin\Debug\TripStackLib.dll");

            Console.WriteLine("1. Addition");
            Console.WriteLine("2. Subtraction");
            Console.WriteLine("3. Square");
            Console.WriteLine("4. Cube");
            Console.WriteLine("Enter operation number: ");

            int choice = Convert.ToInt32(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    //load class
                    Type t = assembly.GetType("TripStackLib.ComplexMath");
                    //get default constructor
                    ConstructorInfo constructor = t.GetConstructor(Type.EmptyTypes);
                    //construct an object
                    object instance = constructor.Invoke(null);
                    //get method to be invoked
                    MethodInfo method = t.GetMethod("Add");

                    Console.WriteLine("Enter first number: ");
                    int x = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter second number: ");
                    int y = Convert.ToInt32(Console.ReadLine());
                    //arrange parameters to be passed to method
                    object[] parameters = { x, y };

                    //invoke method
                    object result = method.Invoke(instance, parameters);
                    //use method's returned data
                    Console.WriteLine("Addition of {0} and {1} is {2}", x, y, result);
                    break;

                case 2:
                    //load class
                    Type t1 = assembly.GetType("TripStackLib.ComplexMath");
                    //get default constructor
                    ConstructorInfo constructor1 = t1.GetConstructor(Type.EmptyTypes);
                    //construct an object
                    object instance1 = constructor1.Invoke(null);
                    //get method to be invoked
                    MethodInfo method1 = t1.GetMethod("Subtract");

                    Console.WriteLine("Enter first number: ");
                    int x1 = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter second number: ");
                    int y1 = Convert.ToInt32(Console.ReadLine());
                    //arrange parameters to be passed to method
                    object[] parameters1 = { x1, y1 };

                    //invoke method
                    object result1 = method1.Invoke(instance1, parameters1);
                    //use method's returned data
                    Console.WriteLine("Subtraction of {0} and {1} is {2}", x1, y1, result1);
                    break;

                case 3:
                    //load class
                    Type t2 = assembly.GetType("TripStackLib.SimpleMath");
                    //get default constructor
                    ConstructorInfo constructor2 = t2.GetConstructor(Type.EmptyTypes);
                    //construct an object
                    object instance2 = constructor2.Invoke(null);
                    //get method to be invoked
                    MethodInfo method2 = t2.GetMethod("Square");

                    Console.WriteLine("Enter first number: ");
                    int x2 = Convert.ToInt32(Console.ReadLine());
                    
                    //arrange parameters to be passed to method
                    object[] parameters2 = { x2 };

                    //invoke method
                    object result2 = method2.Invoke(instance2, parameters2);
                    //use method's returned data
                    Console.WriteLine("Square of {0} is {1}", x2, result2);
                    break;

                case 4:
                    //load class
                    Type t3 = assembly.GetType("TripStackLib.SimpleMath");
                    //get default constructor
                    ConstructorInfo constructor3 = t3.GetConstructor(Type.EmptyTypes);
                    //construct an object
                    object instance3 = constructor3.Invoke(null);
                    //get method to be invoked
                    MethodInfo method3 = t3.GetMethod("Cube");

                    Console.WriteLine("Enter first number: ");
                    int x3 = Convert.ToInt32(Console.ReadLine());

                    //arrange parameters to be passed to method
                    object[] parameters3 = { x3 };

                    //invoke method
                    object result3 = method3.Invoke(instance3, parameters3);
                    //use method's returned data
                    Console.WriteLine("Cube of {0} is {1}", x3, result3);
                    break;
                default:
                    break;
            }
            Console.ReadLine();
        }
    }
}
