﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttributeApp
{
    public class Printer
    {
        public static void Print(object obj)
        {
            if (obj is MyDetails)
            {
                MyDetails md = obj as MyDetails;

                Type t = md.GetType();
                foreach (PrintAttribute pa in t.GetCustomAttributes(false))
                {
                    if(pa.Destination == "Printer")
                    {
                        Console.WriteLine("Output send to printer...");
                    }
                    else
                    {
                        Console.WriteLine(md.FirstName+" "+md.LastName);
                    }
                }
            }
        }
    }
}
