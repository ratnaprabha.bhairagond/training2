﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace SerializationApp
{
    class Program
    {
        static void Main(string[] args)
        {
            
            //create book.txt to store all book information
            FileStream fs = File.OpenWrite(@"c:\training2\books.txt");
            string path = "c:\training2\bookss.txt";
            //create formatter for storing books in books.txt
            BinaryFormatter bf = new BinaryFormatter();

            Book b1 = new Book { BookID = 100, BookName = "Intro to C Sharp", Price = 245 };
            b1.SetPublisher("Packect");
            

            Book b2 = new Book { BookID = 101, BookName = "Pro C Sharp", Price = 550 };
            b2.SetPublisher("APress");

            List<Book> books = new List<Book> { b1, b2};
            
            bf.Serialize(fs, books);

            fs.Close();

            Console.WriteLine("Book stored in Books.txt file!");

            fs = File.OpenRead(@"c:\training2\books.txt");
            
            List<Book> b = (List<Book>)bf.Deserialize(fs);

            fs.Close();

            foreach (var item in b)
            {
                Console.WriteLine(item);
            }
           
        }
    }
}
