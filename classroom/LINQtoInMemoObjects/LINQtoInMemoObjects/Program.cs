﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LINQtoInMemoObjects
{
    class Program
    {
        static void Main(string[] args)
        {
            //NumQuery();
            //ObjectQuery();
            //CreateCustomers();
            //XMLQuery();
            CreateXML();
            Console.ReadLine();
        }
        static void NumQuery()
        {
            var numbers = new int[] { 1, 4, 9, 16, 25, 36};
            var evenNumbers = from p in numbers
                              where (p % 2) == 0
                              select p;
            Console.WriteLine("Result: ");
            foreach (var item in evenNumbers)
            {
                Console.WriteLine(item);
            }
        }

        static IEnumerable<Customer> CreateCustomers()
        {
            //return new List<Customer>
            //{
            //    new Customer{ CustomerID="ALKFI", City="Berlin"}, n    
            //    new Customer{ CustomerID="BONAP", City="Marsielle"},           
            //    new Customer{ CustomerID="CONSH", City="London"},
            //    new Customer{ CustomerID="EASTC", City="London"},
            //    new Customer{ CustomerID="FRANS", City="Torino"},
            //    new Customer{ CustomerID="LONEP", City="Portland"},
            //    new Customer{ CustomerID="NORTS", City="London"},
            //    new Customer{ CustomerID="THEBI", City="Portland"}
            //}; 
            return from c in XDocument.Load("Customers.xml").Descendants("Customers").Descendants()
                   select new Customer { City = c.Attribute("City").Value, CustomerID = c.Attribute("CustomerID").Value };
        }

        static void ObjectQuery()
        {
            var x = from c in CreateCustomers()
                    where c.City == "London"
                    select c;

            foreach (var item in x)
            {
                Console.WriteLine(item);
            }
        }

        static void XMLQuery()
        {
            var doc = XDocument.Load("Customers.xml");
            var result = from c in doc.Descendants("Customer")
                   where c.Attribute("City").Value == "London"
                   select c;
            XElement transformDoc = new XElement("Londoners",
                                    from customer in result
                                    select new XElement("Contact",
                                    new XAttribute("ID", customer.Attribute("CustomerID").Value),
                                    new XAttribute("Name", customer.Attribute("ContractName").Value),
                                    new XAttribute("City", customer.Attribute("City").Value)));
            Console.WriteLine("Result:\n{0}", transformDoc);

            transformDoc.Save("Londoners.xml");
            Console.WriteLine("File Saved!");
        }

        static void CreateXML()
        {
            XElement doc = new XElement("Products",
                           new XElement("Product",
                           new XAttribute("ID", "1"),
                           new XElement("Name", "Foot Ball"),
                           new XElement("Price", "235")),
                           new XElement("Product",
                           new XAttribute("ID", "2"),
                           new XElement("Name", "Bat"),
                           new XElement("Price", "1300")));
            doc.Save("Products.xml");
        }
    }
}
