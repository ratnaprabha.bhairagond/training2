﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadApp
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Thread t = new Thread(new ThreadStart(Test.DoSomething));
            t.Start();
            Thread.Sleep(100);
            t.Abort();
            t.Join();
            Console.WriteLine("Done.....");
            */


            /*
            Console.WriteLine("Main() on thread no: {0}", Thread.CurrentThread.ManagedThreadId);

            Printer a = new Printer('.', 10, "a");
            Printer b = new Printer('*', 50, "b");

            Thread t1 = new Thread(new ThreadStart(a.Print));
            Thread t2 = new Thread(new ThreadStart(b.Print));
            Console.WriteLine("Priority of t1: {0}", t1.Priority);
            Console.WriteLine("Priority of t2: {0}", t2.Priority);

            t1.Start();
            t2.Start();
            Console.WriteLine("Main() is waiting for child to finish...");
            t1.Join();
            t2.Join();

            Console.WriteLine("Main() completed...");
            */


            /*
            Printer p = new Printer();

            //used to pass parameters to method
            ParameterizedThreadStart ps = p.Print;

            Thread t1 = new Thread(ps);
            Thread t2 = new Thread(ps);
            Thread t3 = new Thread(ps);

            t1.Start("Welcome");
            t2.Start("To");
            t3.Start("Tripstack");

            t1.Join();
            t2.Join();
            t3.Join();
            */

            TickTock tt = new TickTock();

            MyThread t1 = new MyThread("Tick", tt);
            MyThread t2 = new MyThread("Tock", tt);

            t1.t.Join();
            t2.t.Join();


            Console.WriteLine("Clock Stopped....");

            Console.ReadLine();
        }
    }
}
