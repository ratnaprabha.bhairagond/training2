﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadApp
{
    public class MyThread
    {
        public Thread t;
        TickTock tt;

        public MyThread(string name, TickTock ttObj)
        {
            t = new Thread(this.Run);
            this.tt = ttObj;
            t.Name = name;
            t.Start();
        }

        public void Run()
        {
            if (t.Name == "Tick")
            {
                for (int i = 0; i < 5; i++)
                {
                    tt.Tick(true);
                }
                tt.Tick(false);
            }
            else
            {
                for (int i = 0; i < 5; i++)
                {
                    tt.Tock(true);
                }
                tt.Tock(false);
            }
        }
    }
}
