﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DisconnectedApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=TrainingDB;Integrated Security=True;Pooling=False"))
            {
                SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM Products", conn);

                DataSet ds = new DataSet();

                //Gets data from DB to Dataset
                adapter.Fill(ds, "Products");

                DataRow row = ds.Tables[0].NewRow();
                row["Name"] = "Tennis Ball";
                row["Description"] = "For child";
                row["Category"] = "Tennis";
                row["Price"] = 312;

                ds.Tables[0].Rows.Add(row);

                //adapter.Update(ds, "Products");
                //Console.WriteLine("Columns {0} in Products", ds.Tables[0].Columns.Count);
             
                //adapter.Fill(ds, "Products");

                for (int i=0; i<ds.Tables[0].Rows.Count; i++)
                {
                    Console.WriteLine("Name: {0}\tDescription: {1}\tCategory: {2}\tPrice: {3}", ds.Tables[0].Rows[i]["Name"], ds.Tables[0].Rows[i]["Description"], ds.Tables[0].Rows[i]["Category"], ds.Tables[0].Rows[i]["Price"]);
                }
                Console.ReadLine();
                
            }
        }
    }
}

