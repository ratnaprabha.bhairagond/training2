﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateApp
{
    delegate int Compute(int a);

    class Program
    {
        static void Main(string[] args)
        {
            //Compute c = new Compute(Square);  //C# 2.0    //Method Registeration
            Compute c = Square;                 //C# 3.0    //Method Register
            var result = c.Invoke(13);
            Console.WriteLine("Result is: {0}", result);
        }

        static int Square(int x)
        {
            return x * x;
        }
    }
}
