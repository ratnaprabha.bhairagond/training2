﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFApp
{
    class Program
    {
        static void Main(string[] args)
        {
            TrainingDBEntities db = new TrainingDBEntities();

            //SELECT
            /*
            foreach (var item in db.Products.Where(p=>p.Price>30))
            {
                Console.WriteLine("ID: {0}\tName: {1}\tDescription: {2}\tCategory: {3}\tPrice: {4}", item.Id, item.Name, item.Description, item.Category, item.Price);
            }
            */

            //INSERT
            /*
            Product prd = new Product { Name = "Tennis Ball", Description = "For child", Category = "Tennis", Price = 321 };
            db.Products.Add(prd);
            */

            //UPDATE
            /*
            var prd = db.Products.Where(p => p.Id == 13).SingleOrDefault();
            prd.Price += 100;
            */

            //DELETE
            var prd = db.Products.Where(p => p.Id == 13).SingleOrDefault();
            db.Products.Remove(prd);
            db.SaveChanges();
            Console.WriteLine("Product Added!!");
            Console.ReadLine();
        }
    }
}
