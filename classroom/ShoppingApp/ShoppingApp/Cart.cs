﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp
{
    class Cart
    {
        public int ProductId { get; set; }
        public int Count { get; set; } = 1;
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Id { get; set; }
    }
}
