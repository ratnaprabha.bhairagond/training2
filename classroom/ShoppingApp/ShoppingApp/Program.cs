﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp
{
    class Program
    {
        static TrainingDBEntities db = new TrainingDBEntities();
        static List<Cart> c = new List<Cart>();
        

        static void Main(string[] args)
        {
            int choice, pid;
            bool i = true;

            while (i)
            {
                Console.Clear();
                Console.WriteLine("\nChoose one of the folowing: ");
                Console.WriteLine("1. List Products");
                Console.WriteLine("2. List by groups");
                Console.WriteLine("3. Add Product to cart");
                Console.WriteLine("4. Edit Quantity");
                Console.WriteLine("5. Cart Details");
                Console.WriteLine("6. Remove Product from Cart");
                Console.WriteLine("7. Cart Summary");
                Console.WriteLine("8. Exit");
                Console.Write("Enter Option: ");
                choice = Convert.ToInt32(Console.ReadLine());
                Console.Clear();
                switch (choice)
                {
                    case 1:
                        ListProducts();
                        Console.ReadKey();
                        break;
                    case 2:
                        ListCategory();
                        Console.ReadKey();
                        break;
                    case 3:
                        Console.Write("\nEnter Product ID: ");
                        pid = Convert.ToInt32(Console.ReadLine());
                        AddProduct(pid);
                        Console.ReadKey();
                        break;
                    case 4:
                        Console.Write("\nEnter Product ID: ");
                        pid = Convert.ToInt32(Console.ReadLine());
                        EditQuantity(pid);
                        Console.ReadKey();
                        break;
                    case 5:
                        CartDetails();
                        Console.ReadKey();
                        break;
                    case 6:
                        Console.Write("\nEnter Product ID: ");
                        pid = Convert.ToInt32(Console.ReadLine());
                        RemoveProduct(pid);
                        Console.ReadKey();
                        break;
                    case 7:
                        CartSumm();
                        Console.ReadLine();
                        break;
                    case 8:
                        Console.WriteLine("Exiting.. (Enter any key)");
                        Console.ReadKey();
                        i = false;
                        break;
                    default:
                        break;
                }
            }
            
        }


        static void ListProducts()
        {
            foreach (var item in db.Products)
            {
                Console.WriteLine("\nID:\t\t{0}\nName:\t\t{1}\nDescription:\t{2}\nCategory:\t{3}\nPrice:\t\t{4}", item.Id, item.Name, item.Description, item.Category, item.Price);
                Console.WriteLine();
            }
        }

        static void AddProduct(int pid)
        {
            Product p = null;
            Cart crt = null;
            p=db.Products.Where(x=> x.Id==pid).SingleOrDefault();
            crt = c.Find(x => x.ProductId == pid);
            if (p==null)
            {
                Console.WriteLine("\nProduct ID not present!");
            }
            else
            {
                
                if(crt == null)
                {
                    c.Add(new Cart { ProductId = p.Id, Name=p.Name, Price=p.Price, Id=p.Id});
                }
                else
                {
                    crt.Count++;
                }
                Console.WriteLine("\nSuccessfully Added!");
            }
        }


        static void ListCategory()
        {
            int c;
            string category=null;
            Console.WriteLine("\n1. Waterports");
            Console.WriteLine("2. Soccer");
            Console.WriteLine("3. Chess");
            Console.Write("Enter Category Choice: ");
            c = Convert.ToInt32(Console.ReadLine());
            switch (c)
            {
                case 1:
                    category = "Waterports";
                    break;
                case 2:
                    category = "Soccer";
                    break;
                case 3:
                    category = "Chess";
                    break;
                default:
                    break;
            }
            foreach (var item in db.Products)
            {
                if(item.Category == category)
                {
                    Console.WriteLine("\nID:\t\t{0}\nName:\t\t{1}\nDescription:\t{2}\nCategory:\t{3}\nPrice:\t\t{4}", item.Id, item.Name, item.Description, item.Category, item.Price);
                    Console.WriteLine();
                }
            }
        }


        static void CartDetails()
        {
            Console.WriteLine("\nID\tName\t\tPrice\t\tQuantity\tTotal");
            foreach (var prd in c)
            {
                Console.WriteLine("\n{4}\t{0}\t\t{1}\t\t{2}\t\t{3}", prd.Name, prd.Price, prd.Count, prd.Count*prd.Price, prd.Id);
            }
        }

        static void RemoveProduct(int pid)
        {
            Cart crt = null;
            crt = c.Find(x => x.ProductId == pid); 
            if(crt==null)
            {
                Console.WriteLine("Product ID not present!");
            }
            else
            {
                c.Remove(crt);
            }
        }

        static void CartSumm()
        {
            decimal total = 0;
            int quant = 0;
            foreach (var prd in c)
            {
                total += prd.Price * prd.Count;
                quant += prd.Count;
            }
            Console.WriteLine("\nTotal Items: {0}", quant);
            Console.WriteLine("Total Price: {0}", total);
        }

        static void EditQuantity(int pid)
        {
            Cart crt = null;
            int q;
            crt = c.Find(x => x.ProductId == pid);
            if (crt == null)
            {
                Console.WriteLine("Product ID not present!");
            }
            else
            {
                Console.WriteLine("\nEnter Quantity: ");
                q = Convert.ToInt32(Console.ReadLine());
                crt.Count = q;
            }
        }

        
    }
}
