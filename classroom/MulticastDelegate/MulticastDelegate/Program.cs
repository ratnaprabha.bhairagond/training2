﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MulticastDelegate
{
    class Program
    {
        delegate void Compute(int a);

        static void Main(string[] args)
        {
            Compute c = Cube;
            c += Square;
            c.Invoke(13);
        }

        static void Square(int x)
        {
            Console.WriteLine("Square of {0} is {1}", x, (x * x));
        }

        static void Cube(int y)
        {
            Console.WriteLine("Cube of {0} is {1}", y, (y * y * y));
        }
    }
}
