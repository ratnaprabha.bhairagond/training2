﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Authentication
{
    delegate void LoginDelegate(LoginEventArgs a);
    class Login
    {
        public event LoginDelegate LoginSuccessfull;
        public event LoginDelegate LoginFailed;

        public static bool Authenticate(User u)
        {
            if(u.UserName=="flight" && u.Password=="flight")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void LoginResult(User u)
        {
            LoginEventArgs e = new LoginEventArgs();
            e.UserName = u.UserName;
            e.Password = u.Password;
            if(Authenticate(u)==true)
            {
                LoginSuccessfull(e);
            }
            else
            {
                LoginFailed(e);
            }
        }
    }
}
