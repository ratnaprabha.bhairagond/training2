﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Authentication
{
    class Program
    {
        static void Main(string[] args)
        {
            Login l = new Login();
            l.LoginSuccessfull += OnSuccess;
            l.LoginFailed += OnFail;
            User u1 = new User();
            User u2 = new User();
            u1.UserName = "flight";
            u1.Password = "flight";
            u2.UserName = "abc";
            u2.Password = "efg";
            l.LoginResult(u1);
            l.LoginResult(u2);

        }

        private static void OnSuccess(LoginEventArgs x)
        {
            Console.WriteLine("UserName: {0}\nPassword: {1}", x.UserName, x.Password);
            Console.WriteLine("Successful!!");
        }

        private static void OnFail(LoginEventArgs y)
        {
            Console.WriteLine("UserName: {0}\nPassword: {1}", y.UserName, y.Password);
            Console.WriteLine("Failed!!");
        }
    }

    class LoginEventArgs
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
