﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6Prob2
{
    class Program
    {
        static void Main(string[] args)
        {
            int c, did;
            Manager[] M = new Manager[20];
            President[] P = new President[20];
            string name;
            double bs, km;
            bool i = true;
            while(i)
            {
                Console.WriteLine("\nChoose one of the following: ");
                Console.WriteLine("1. Enter Manager");
                Console.WriteLine("2. Enter President");
                Console.WriteLine("3. Display Managers");
                Console.WriteLine("4. Display Presidents");
                Console.WriteLine("5. Exit");
                Console.WriteLine("Enter choice: ");
                c = Convert.ToInt32(Console.ReadLine());
                switch(c)
                {
                    case 1:
                        Console.WriteLine("\nEnter Employee Name: ");
                        name = Console.ReadLine();
                        Console.WriteLine("Enter Basic Salary: ");
                        bs = Convert.ToDouble(Console.ReadLine());
                        Console.WriteLine("Enter Department ID: ");
                        did = Convert.ToInt32(Console.ReadLine());
                        M[Manager.ManageCount()] = new Manager(name, bs, did);
                        break;
                    case 2:
                        Console.WriteLine("\nEnter Employee Name: ");
                        name = Console.ReadLine();
                        Console.WriteLine("Enter Basic Salary: ");
                        bs = Convert.ToDouble(Console.ReadLine());
                        Console.WriteLine("Enter Department ID: ");
                        did = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Enter Kilometers Travelled: ");
                        km = Convert.ToDouble(Console.ReadLine());
                        P[President.PresCount()] = new President(name, bs, did, km);
                        break;
                    case 3:
                        Console.WriteLine("\nManagers:\n");
                        for (int k = 0; k < Manager.ManageCount(); k++)
                        {
                            Console.WriteLine(M[k].ToString());
                        }
                        break;
                    case 4:
                        Console.WriteLine("\nPresidents:\n");
                        for (int j = 0; j < President.PresCount(); j++)
                        {
                            Console.WriteLine(P[j].ToString());
                        }
                        break;
                    case 5:
                        Console.WriteLine("\nExiting..");
                        i = false;
                        break;
                    default:
                        Console.WriteLine("\nEnter valid option only!");
                        break;
                }
            }
        }
    }
}
