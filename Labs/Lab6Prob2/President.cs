﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6Prob2
{
    class President : Employee
    {
        private double KMTravel;
        private double TourAllow;
        private int TeleAllow;
        private static int count;

        public President(string name, double bsalary, int deptid, double kmtravel) : base(name, bsalary, deptid)
        {
            count++;
            KMTravel = kmtravel;
            TourAllow = (8 * kmtravel);
            TeleAllow = 2000;
        }

        public override string ToString()
        {
            return base.ToString() + "\nDesignation: \t\tPresident"+"\nKilometers Travelled: \t" + KMTravel + "\nTour Allowance: \t" + TeleAllow + "\nTelephone Allowance: \t" + TeleAllow;
        }

        public static int PresCount()
        {
            return count;
        }
    }
}
