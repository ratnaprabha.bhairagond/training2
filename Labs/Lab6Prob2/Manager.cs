﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6Prob2
{
    class Manager : Employee
    {
        private double PetrolAllow;
        private double FoodAllow;
        private double OtherAllow;
        private static int count;

        public Manager(string name, double bsalary, int deptid) : base(name, bsalary, deptid)
        {
            count++;
            PetrolAllow = (0.08 * bsalary);
            FoodAllow = (0.13 * bsalary);
            OtherAllow = (0.03 * bsalary);
        }

        public override string ToString()
        {
            return base.ToString() + "\nDesignation: \t\tManager"+"\nPetrol Allowance: \t" + PetrolAllow + "\nFood Allowance: \t" + FoodAllow + "\nOther Allowance: \t" + OtherAllow;
        }

        public static int ManageCount()
        {
            return count;
        }
    }
}
