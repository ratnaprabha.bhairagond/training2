﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6Prob2
{
    class Employee
    {
        protected int EmpID;
        protected string EmpName;
        protected double BasicSalary;
        protected double HRA;
        protected double DA;
        protected double ProFund;
        protected double GrossSalary;
        protected double NetSalary;
        protected int DepartID;
        private static int count;

        public Employee(string name, double bsalary, int deptid)
        {
            count++;
            EmpID = 100 + count;
            EmpName = name;
            BasicSalary = bsalary;
            DepartID = deptid;
            HRA = (0.8 * BasicSalary);
            ProFund = (0.12 * BasicSalary);
            GrossSalary = BasicSalary + HRA + 1250;
            NetSalary = GrossSalary - (1500 + ProFund);
        }

        public static int EmpCount()
        {
            return count;
        }

        public override string ToString()
        {
            return "\nEmployee ID: \t\t" + EmpID + "\nEmployee Name: \t\t" + EmpName + "\nBasic Salary: \t\t" + BasicSalary + "\nHRA: \t\t\t" + HRA + "\nProvident Fund: \t" + ProFund + "\nGross Salary: \t\t" + GrossSalary + "\nNet Salary: \t\t" + NetSalary + "\nDepartment ID: \t\t" + DepartID;
        }
    }
}
