﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    class Employee
    {
        private int EmpID;
        private string EmpName;
        private string Design;
        private double BasicSalary;
        private double HRA;
        private double ProFund;
        private double GrossSalary;
        private double NetSalary;
        private int DepartID;
        private static int count;

        public Employee(string name, string design, double bsalary, int deptid)
        {
            count++;
            EmpID = 100 + count;
            EmpName = name;
            Design = design;
            BasicSalary = bsalary;
            DepartID = deptid;
            CalSalary();
        }

        private void CalSalary()
        {
            if(Design=="Employee")
            {
                HRA = (0.8 * BasicSalary);
                ProFund = (0.12 * BasicSalary);
                GrossSalary = BasicSalary + HRA + 1250;
                NetSalary = GrossSalary - (1500 + ProFund);
            }
            else if (Design=="Manager")
            {
                HRA = (0.10 * BasicSalary);
                ProFund = (0.15 * BasicSalary);
                GrossSalary = BasicSalary + HRA + 2000;
                NetSalary = GrossSalary - (2000 + ProFund);
            }
            else
            {
                HRA = (0.15 * BasicSalary);
                ProFund = (0.20 * BasicSalary);
                GrossSalary = BasicSalary + HRA + 3000;
                NetSalary = GrossSalary - (2500 + ProFund);
            }
        }

        public void PrintEmp()
        {
            Console.WriteLine("\nEmployee ID: \t\t{0}", EmpID);
            Console.WriteLine("Employee Name: \t\t{0}", EmpName);
            Console.WriteLine("Designation: \t\t{0}", Design);
            Console.WriteLine("Basic Salary: \t\t{0}", BasicSalary);
            Console.WriteLine("HRA: \t\t\t{0}", HRA);
            Console.WriteLine("Provident Fund: \t{0}", ProFund);
            Console.WriteLine("Gross Salary: \t\t{0}", GrossSalary);
            Console.WriteLine("Net Salary: \t\t{0}", NetSalary);
            Console.WriteLine("Department ID: \t\t{0}\n", DepartID);
        }

        public static int EmpCount()
        {
            return count;
        }
    }
}
