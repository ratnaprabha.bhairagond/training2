﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7
{
    class Program
    {
        static void Main(string[] args)
        {
            Shape S = null;
            bool i = true, f;
            int ch, c;
            double l, b, r;
            while(i)
            {
                f = true;
                Console.WriteLine("\nChoose one of the following: ");
                Console.WriteLine("1. Enter Shape");
                Console.WriteLine("2. Calculate Area");
                Console.WriteLine("3. Exit");
                Console.WriteLine("Enter option: ");
                c = Convert.ToInt32(Console.ReadLine());
                switch(c)
                {
                    case 1:
                        while (f)
                        {
                            Console.WriteLine("\nChoose Shape: ");
                            Console.WriteLine("1. Circle");
                            Console.WriteLine("2. Rectangle");
                            Console.WriteLine("Enter option: ");
                            ch = Convert.ToInt32(Console.ReadLine());
                            switch (ch)
                            {
                                case 1:
                                    Console.WriteLine("\nEnter Radius: ");
                                    r = Convert.ToDouble(Console.ReadLine());
                                    S = new Circle(r);
                                    f = false;
                                    break;
                                case 2:
                                    Console.WriteLine("\nEnter Length: ");
                                    l = Convert.ToDouble(Console.ReadLine());
                                    Console.WriteLine("\nEnter Breath: ");
                                    b = Convert.ToDouble(Console.ReadLine());
                                    S = new Rectangle(l, b);
                                    f = false;
                                    break;
                                default:
                                    Console.WriteLine("Invalid Option! Try Again!");
                                    break;
                            }
                        }
                        break;
                    case 2:
                        if (S!=null)
                            Console.WriteLine("\nArea: {0}", S.Area());
                        else
                            Console.WriteLine("\nFirst Select Option 1!");
                        break;
                    case 3:
                        Console.WriteLine("\nExiting...");
                        i = false;
                        break;
                    default:
                        Console.WriteLine("\nInvalid Option! Try Again!");
                        break;
                }
            }
        }
    }
}
