﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7
{
    class Rectangle : Shape
    {
        private double Length;
        private double Breath;

        public Rectangle(double a, double b)
        {
            Length = a;
            Breath = b;
        }

        public override double Area()
        {
            return Length * Breath;
        }
    }
}
