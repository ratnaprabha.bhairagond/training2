﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8
{
    class Rectangle : Shape
    {
        private double Length;
        private double Breath;

        public Rectangle(double a, double b)
        {
            Length = a;
            Breath = b;
        }

        public override double Area()
        {
            return Length * Breath;
        }

        public override void Print()
        {
            Console.WriteLine("\nLenght: {0}", Length);
            Console.WriteLine("Breath: {0}", Breath);
            Console.WriteLine("Area: \t{0}", Area());
        }
    }
}
