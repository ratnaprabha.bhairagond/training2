﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8
{
    class Date : IPrintable
    {
        private int mDay;
        private int mMonth;
        private int mYear;

        public Date()
        {
            Console.WriteLine("Day: ");
            mDay = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Month: ");
            mMonth = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Year: ");
            mYear = Convert.ToInt32(Console.ReadLine());
        }

        public Date(int d, int m, int y)
        {
            mDay = d;
            mMonth = m;
            mYear = y;
        }

        public void Print()
        {
            Console.WriteLine(mDay + "-" + mMonth + "-" + mYear + "\n");
        }
    }
}
