﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8
{
    class Circle : Shape
    {
        private double Radius;

        public Circle(double r)
        {
            Radius = r;
        }

        public override double Area()
        {
            return 3.142 * Radius * Radius;
        }

        public override void Print()
        {
            Console.WriteLine("\nRadius: {0}", Radius);
            Console.WriteLine("Area: \t{0}", Area());
        }
    }
}
