﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8
{
    class Program
    {
        static void Main(string[] args)
        {
            IPrintable P=null;
            int c, ch, did;
            bool i = true, j, f, g;
            string name, des="";
            double bs, l, r, b;
            while(i)
            {
                j = true;
                Console.WriteLine("\nChoose one of the following: ");
                Console.WriteLine("1. Create Object");
                Console.WriteLine("2. Print Object");
                Console.WriteLine("3. Exit");
                Console.WriteLine("Enter option: ");
                c = Convert.ToInt32(Console.ReadLine());
                switch(c)
                {
                    case 1:
                        while(j)
                        {
                            f = true;
                            g = true;
                            Console.WriteLine("\nChoose Object: ");
                            Console.WriteLine("1. Date");
                            Console.WriteLine("2. Employee");
                            Console.WriteLine("3. Shape");
                            Console.WriteLine("Enter option: ");
                            ch = Convert.ToInt32(Console.ReadLine());
                            switch(ch)
                            {
                                case 1:
                                    P = new Date();
                                    j = false;
                                    break;
                                case 2:
                                    Console.WriteLine("\nEnter Employee Name: ");
                                    name = Console.ReadLine();
                                    while (f)
                                    {
                                        Console.WriteLine("\nEnter Designation: ");
                                        Console.WriteLine("1. Employee");
                                        Console.WriteLine("2. Manager");
                                        Console.WriteLine("3. President");
                                        Console.WriteLine("Enter choice: ");
                                        ch = Convert.ToInt32(Console.ReadLine());
                                        switch (ch)
                                        {
                                            case 1:
                                                des = "Employee";
                                                f = false;
                                                break;
                                            case 2:
                                                des = "Manager";
                                                f = false;
                                                break;
                                            case 3:
                                                des = "President";
                                                f = false;
                                                break;
                                            default:
                                                Console.WriteLine("\nInvalid Option! Try Again!");
                                                break;
                                        }
                                    }
                                    Console.WriteLine("\nEnter Basic Salary: ");
                                    bs = Convert.ToDouble(Console.ReadLine());
                                    Console.WriteLine("\nEnter Department ID: ");
                                    did = Convert.ToInt32(Console.ReadLine());
                                    P = new Employee(name, des, bs, did);
                                    j = false;
                                    break;
                                case 3:
                                    while (g)
                                    {
                                        Console.WriteLine("\nChoose Shape: ");
                                        Console.WriteLine("1. Circle");
                                        Console.WriteLine("2. Rectangle");
                                        Console.WriteLine("Enter option: ");
                                        ch = Convert.ToInt32(Console.ReadLine());
                                        switch (ch)
                                        {
                                            case 1:
                                                Console.WriteLine("\nEnter Radius: ");
                                                r = Convert.ToDouble(Console.ReadLine());
                                                P = new Circle(r);
                                                g = false;
                                                break;
                                            case 2:
                                                Console.WriteLine("\nEnter Length: ");
                                                l = Convert.ToDouble(Console.ReadLine());
                                                Console.WriteLine("\nEnter Breath: ");
                                                b = Convert.ToDouble(Console.ReadLine());
                                                P = new Rectangle(l, b);
                                                g = false;
                                                break;
                                            default:
                                                Console.WriteLine("\nInvalid Option! Try Again!");
                                                break;
                                        }
                                    }
                                    j = false;
                                    break;
                                default:
                                    Console.WriteLine("\nInvalid Option! Try Again!");
                                    break;
                            }
                        }
                        break;
                    case 2:
                        P.Print();
                        break;
                    case 3:
                        Console.WriteLine("\nExiting...");
                        i = false;
                        break;
                    default:
                        Console.WriteLine("\nInvalid Option! Try Again!");
                        break;
                }
            }
        }
    }
}
