﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2Prob1
{
    class Program
    {
        static void Main(string[] args)
        {
            int d, m, y;

            Console.WriteLine("\nEnter Date 1:");
            Date d1 = new Date();

            Console.WriteLine("\nEnter Date 2:\nDay: ");
            d = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Month: ");
            m = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Year: ");
            y = Convert.ToInt32(Console.ReadLine());

            Date d2 = new Date(d, m, y);
            //Date d3 = new Date { mDay = 21, mMonth=03, mYear=2007 };

            Console.WriteLine("\n\nDate 1: ");
            d1.PrintDate();
            Console.WriteLine("Date 2: ");
            d2.PrintDate();
            //d3.PrintDate();
        }
    }
}
