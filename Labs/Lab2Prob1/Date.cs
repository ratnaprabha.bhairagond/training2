﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2Prob1
{
    class Date
    {
        private int mDay { get; set; }
        private int mMonth { get; set; }
        private int mYear { get; set; }

        public Date()
        {
            Console.WriteLine("Day: ");
            mDay = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Month: ");
            mMonth = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Year: ");
            mYear = Convert.ToInt32(Console.ReadLine());
        }

        public Date(int d, int m, int y)
        {
            mDay = d;
            mMonth = m;
            mYear = y;
        }

        public void PrintDate()
        {
            Console.WriteLine(mDay+"-"+mMonth+"-"+mYear+"\n");
        }
    }
}
