﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee[] E = new Employee[20];
            int c, ch, did;
            bool i = true, f;
            string name, des="";
            double bs;
            while (i)
            {
                f = true;
                Console.WriteLine("\nChoose one of the following: ");
                Console.WriteLine("1. Enter Employee");
                Console.WriteLine("2. Display Employees");
                Console.WriteLine("3. Exit");
                Console.WriteLine("Enter choice: ");
                c = Convert.ToInt32(Console.ReadLine());
                switch (c)
                {
                    case 1:
                        Console.WriteLine("\nEnter Employee Name: ");
                        name = Console.ReadLine();
                        while (f)
                        {
                            Console.WriteLine("\nEnter Designation: ");
                            Console.WriteLine("1. Employee");
                            Console.WriteLine("2. Manager");
                            Console.WriteLine("3. President");
                            Console.WriteLine("Enter choice: ");
                            ch = Convert.ToInt32(Console.ReadLine());
                            switch(ch)
                            {
                                case 1:
                                    des = "Employee";
                                    f = false;
                                    break;
                                case 2:
                                    des = "Manager";
                                    f = false;
                                    break;
                                case 3:
                                    des = "President";
                                    f = false;
                                    break;
                                default:
                                    Console.WriteLine("\nInvalid Option! Try Again!");
                                    break;
                            }
                        }
                        Console.WriteLine("\nEnter Basic Salary: ");
                        bs = Convert.ToDouble(Console.ReadLine());
                        Console.WriteLine("\nEnter Department ID: ");
                        did = Convert.ToInt32(Console.ReadLine());
                        E[Employee.EmpCount()] = new Employee(name, des, bs, did);
                        break;
                    case 2:
                        Console.WriteLine("\nEmployees:\n");
                        for (int k = 0; k < Employee.EmpCount(); k++)
                        {
                            Console.WriteLine(E[k].ToString());
                        }
                        break;
                    case 3:
                        Console.WriteLine("\nExiting..");
                        i = false;
                        break;
                    default:
                        Console.WriteLine("\nEnter valid option only!");
                        break;
                }
            }
        }
    }
}
