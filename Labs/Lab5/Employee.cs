﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5
{
    class Employee
    {
        private int EmpID;
        private string EmpName;
        private string Design;
        private double BasicSalary;
        private double HRA;
        private double DA;
        private double ProFund;
        private double GrossSalary;
        private double NetSalary;
        private int DepartID;
        private static int count;

        public Employee(string name, string design, double bsalary, int deptid)
        {
            count++;
            EmpID = 100 + count;
            EmpName = name;
            Design = design;
            BasicSalary = bsalary;
            DepartID = deptid;
            HRA = (0.8 * BasicSalary);
            ProFund = (0.12 * BasicSalary);
            GrossSalary = BasicSalary + HRA + 1250;
            NetSalary = GrossSalary - (1500 + ProFund);
        }

        public static int EmpCount()
        {
            return count;
        }

        public override string ToString()
        {
            return "\nEmployee ID: \t\t" + EmpID + "\nEmployee Name: \t\t" + EmpName + "\nDesignation: \t\t" + Design + "\nBasic Salary: \t\t" + BasicSalary + "\nHRA: \t\t\t" + HRA + "\nProvident Fund: \t" + ProFund + "\nGross Salary: \t\t" + GrossSalary + "\nNet Salary: \t\t" + NetSalary + "\nDepartment ID: \t\t" + DepartID;
        }
    }
}
