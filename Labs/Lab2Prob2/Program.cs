﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2Prob2
{
    class Program
    {
        static void Main(string[] args)
        {
            Department[] D = new Department[20];
            Employee[] E = new Employee[20];
            int c, ch, did;
            string name, location, des="";
            bool i = true, f;
            double bs;
            while(i)
            {
                f = true;
                Console.WriteLine("\nChoose one of the following: ");
                Console.WriteLine("1. Enter Department");
                Console.WriteLine("2. Enter Employee");
                Console.WriteLine("3. Display Departments");
                Console.WriteLine("4. Display Employees");
                Console.WriteLine("5. Exit");
                Console.WriteLine("Enter choice: ");
                c = Convert.ToInt32(Console.ReadLine());
                switch(c)
                {
                    case 1:
                        Console.WriteLine("\nEnter Department Name: ");
                        name = Console.ReadLine();
                        Console.WriteLine("Enter Location: ");
                        location = Console.ReadLine();
                        D[Department.DepCount()] = new Department(name, location);
                        break;
                    case 2:
                        Console.WriteLine("\nEnter Employee Name: ");
                        name = Console.ReadLine();
                        while (f)
                        {
                            Console.WriteLine("\nEnter Designation: ");
                            Console.WriteLine("1. Employee");
                            Console.WriteLine("2. Manager");
                            Console.WriteLine("3. President");
                            Console.WriteLine("Enter choice: ");
                            ch = Convert.ToInt32(Console.ReadLine());
                            switch (ch)
                            {
                                case 1:
                                    des = "Employee";
                                    f = false;
                                    break;
                                case 2:
                                    des = "Manager";
                                    f = false;
                                    break;
                                case 3:
                                    des = "President";
                                    f = false;
                                    break;
                                default:
                                    Console.WriteLine("\nInvalid Option! Try Again!");
                                    break;
                            }
                        }
                        Console.WriteLine("\nEnter Basic Salary: ");
                        bs = Convert.ToDouble(Console.ReadLine());
                        Console.WriteLine("\nEnter Department ID: ");
                        did = Convert.ToInt32(Console.ReadLine());
                        E[Employee.EmpCount()] = new Employee(name, des, bs, did);
                        break;
                    case 3:
                        Console.WriteLine("\nDepartments:\n");
                        for(int j = 0; j  <Department.DepCount(); j++)
                        {
                            D[j].PrintDept();
                        }
                        break;
                    case 4:
                        Console.WriteLine("\nEmployees:\n");
                        for (int k = 0; k < Employee.EmpCount(); k++)
                        {
                            E[k].PrintEmp();
                        }
                        break;
                    case 5:
                        Console.WriteLine("\nExiting..");
                        i = false;
                        break;
                    default:
                        Console.WriteLine("\nEnter valid option only!");
                        break;
                }
            }
        }
    }
}
