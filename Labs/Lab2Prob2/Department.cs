﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2Prob2
{
    class Department
    {
        private int DepartID;
        private string DepartName;
        private string Location;
        private static int count;

        public Department(string name, string location)
        {
            DepartID = ++count;
            DepartName = name;
            Location = location;
        }

        public void PrintDept()
        {
            Console.WriteLine("\nDepartment ID: \t\t{0}", DepartID);
            Console.WriteLine("Department Name: \t{0}", DepartName);
            Console.WriteLine("Location: \t\t{0}\n", Location);
        }

        public static int DepCount()
        {
            return count;
        }
    }
}
