﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Globalization;

namespace FlightAssignment
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> list = new List<string>();
            string format = "M/dd/yyyy HH:mm:ss";
            string o = args[1];
            string d = args[3];

            StreamReader sr1 = new StreamReader(@"C:\training2\FlightAssignment\Provider1.txt");
            StreamReader sr2 = new StreamReader(@"C:\training2\FlightAssignment\Provider2.txt");
            StreamReader sr3 = new StreamReader(@"C:\training2\FlightAssignment\Provider3.txt");

            while (!sr1.EndOfStream)
            {
                list.Add(sr1.ReadLine());
            }
            list.RemoveAt(0);
            list = list.Distinct().ToList();

            using (SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Flights;Integrated Security=True;Pooling=False"))
            {
                conn.Open();
                SqlCommand command = new SqlCommand();
                command.CommandText = string.Format("DELETE FROM JetAirways");
                command.CommandType = System.Data.CommandType.Text;
                command.Connection = conn;
                command.ExecuteNonQuery();
                foreach (var item in list)
                {
                    string[] data = item.Split(',');
                    command.CommandText = string.Format("INSERT INTO JetAirways VALUES('{0}', '{1}', '{2}', '{3}', {4})", data[0], data[1], data[2], data[3], Convert.ToDouble(data[4].TrimStart('$')));
                    command.CommandType = System.Data.CommandType.Text;
                    command.Connection = conn;
                    command.ExecuteNonQuery();
                }
                conn.Close();
            }

            list.Clear();

            while (!sr2.EndOfStream)
            {
                list.Add(sr2.ReadLine());
            }
            list.RemoveAt(0);
            list = list.Distinct().ToList();

            using (SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Flights;Integrated Security=True;Pooling=False"))
            {
                conn.Open();
                SqlCommand command = new SqlCommand();
                command.CommandText = string.Format("DELETE FROM SpiceJet");
                command.CommandType = System.Data.CommandType.Text;
                command.Connection = conn;
                command.ExecuteNonQuery();
                foreach (var item in list)
                {
                    string[] data = item.Split(',');
                    command.CommandText = string.Format("INSERT INTO SpiceJet VALUES('{0}', '{1}', '{2}', '{3}', {4})", data[0], data[1], data[2], data[3], Convert.ToDouble(data[4].TrimStart('$')));
                    command.CommandType = System.Data.CommandType.Text;
                    command.Connection = conn;
                    command.ExecuteNonQuery();
                }
                conn.Close();
            }

            list.Clear();

            while (!sr3.EndOfStream)
            {
                list.Add(sr3.ReadLine());
            }
            list.RemoveAt(0);
            list = list.Distinct().ToList();

            using (SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Flights;Integrated Security=True;Pooling=False"))
            {
                conn.Open();
                SqlCommand command = new SqlCommand();
                command.CommandText = string.Format("DELETE FROM Qatar");
                command.CommandType = System.Data.CommandType.Text;
                command.Connection = conn;
                command.ExecuteNonQuery();
                foreach (var item in list)
                {
                    string[] data = item.Split('|');
                    command.CommandText = string.Format("INSERT INTO Qatar VALUES('{0}', '{1}', '{2}', '{3}', {4})", data[0], data[1], data[2], data[3], Convert.ToDouble(data[4].TrimStart('$')));
                    command.CommandType = System.Data.CommandType.Text;
                    command.Connection = conn;
                    command.ExecuteNonQuery();
                }
                conn.Close();
            }

            using (SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Flights;Integrated Security=True;Pooling=False"))
            {
                conn.Open();
                SqlCommand command = new SqlCommand();
                command.CommandText = string.Format("(SELECT * FROM JetAirways WHERE Origin='{0}' AND Destination='{1}' UNION SELECT * FROM SpiceJet  WHERE Origin='{0}' AND Destination='{1}' UNION SELECT * FROM Qatar  WHERE Origin='{0}' AND Destination='{1}') ORDER BY Price, 'Departure Time'", o, d);
                command.CommandType = System.Data.CommandType.Text;
                command.Connection = conn;
                SqlDataReader reader = command.ExecuteReader();
                while(reader.Read())
                {
                    Console.WriteLine(String.Format("{0} ---> {2} ({1} ---> {3}) - ${4}", reader["Origin"], Convert.ToDateTime((reader["Departure Time"])).ToString(format, CultureInfo.InvariantCulture), reader["Destination"], Convert.ToDateTime((reader["Destination Time"])).ToString(format, CultureInfo.InvariantCulture), reader["Price"]));
                }
                conn.Close();
            }
           

            sr1.Close();
            sr2.Close();
            sr3.Close();
        }
    }
}
