﻿using FlightNetLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Colours
{
    class Program
    {
        static void Main(string[] args)
        {
            FlightColors fc = new FlightColors { ID = Convert.ToInt32(args[0]), Name = args[1] };
            Console.WriteLine(fc);
        }
    }
}
