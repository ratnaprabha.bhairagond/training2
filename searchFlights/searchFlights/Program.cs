﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Globalization;

namespace searchFlights
{
    class Program
    {
        static void Main(string[] args)
        {

            //List of Providers
            string[] z = { "JetAirways", "SpiceJet", "Qatar" };


            List<string> list = new List<string>();
            string format = "M/dd/yyyy HH:mm:ss";
            string q = "", v="";


            //Input Command Line Arguments
            int i = Array.IndexOf(args, "-o");
            string o = args[i + 1];
            int j = Array.IndexOf(args, "-d");
            string d = args[j + 1];
            if(i<0)
            {
                Console.WriteLine("Enter Origin!");
                return;
            }
            if (j < 0)
            {
                Console.WriteLine("Enter Destination!");
                return;
            }


            using (SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Flights;Integrated Security=True;Pooling=False"))
            {

                conn.Open();


                //Traverse through Providers
                foreach (var x in z)
                {

                    StreamReader sr = new StreamReader(string.Format("{0}.txt", x));
                    list.Clear();


                    //Traversing File and saving distinct entry into list
                    while (!sr.EndOfStream)
                    {
                        list.Add(sr.ReadLine());
                    }
                    sr.Close();
                    list.RemoveAt(0);
                    list = list.Distinct().ToList();

                    
                    try
                    {
                        //Create Table
                        using (SqlCommand c = new SqlCommand(string.Format("CREATE TABLE {0}(Origin nvarchar(50), [Departure Time] datetime, Destination nvarchar(50), [Destination Time] datetime, Price decimal(18, 2))", x), conn))
                            c.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        //Deleting data in table if table exists
                        SqlCommand co = new SqlCommand();
                        co.CommandText = string.Format("DELETE FROM {0}", x);
                        co.CommandType = System.Data.CommandType.Text;
                        co.Connection = conn;
                        co.ExecuteNonQuery();
                    }


                    string[] data = null;
                    SqlCommand command = new SqlCommand();


                    //Traversing list and inserting into table
                    foreach (var item in list)
                    {
                        if (item.Contains(','))
                            data = item.Split(',');
                        else if (item.Contains('|'))
                            data = item.Split('|');
                        command.CommandText = string.Format("INSERT INTO {5} VALUES('{0}', '{1}', '{2}', '{3}', {4})", data[0], data[1], data[2], data[3], Convert.ToDouble(data[4].TrimStart('$')), x);
                        command.CommandType = System.Data.CommandType.Text;
                        command.Connection = conn;
                        command.ExecuteNonQuery();              
                    }
                    

                    //Creating select command
                    q = q + v + string.Format("SELECT * FROM {2} WHERE Origin='{0}' AND Destination='{1}' ", o, d, x);
                    v = "UNION ";
                }


                q = q + "ORDER BY Price, 'Departure Time'";


                //Select from table
                SqlCommand comm = new SqlCommand();
                comm.CommandText = q;
                comm.CommandType = System.Data.CommandType.Text;
                comm.Connection = conn;
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    //Output Format
                    Console.WriteLine(String.Format("{0} ---> {2} ({1} ---> {3}) - ${4}", reader["Origin"], Convert.ToDateTime((reader["Departure Time"])).ToString(format, CultureInfo.InvariantCulture), reader["Destination"], Convert.ToDateTime((reader["Destination Time"])).ToString(format, CultureInfo.InvariantCulture), reader["Price"]));
                }


                conn.Close();

            }
        }
    }
}
